package frantisekhanzlikbl.kurisu.ast.node

interface UnaryOperation : Expression {
	data class Minus(override val operand: Expression) : UnaryOperation

	val operand: Expression
}
