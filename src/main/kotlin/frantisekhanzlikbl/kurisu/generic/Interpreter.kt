package frantisekhanzlikbl.kurisu.generic

import frantisekhanzlikbl.kurisu.generic.ast.Node

interface Interpreter<RootNode : Node> {
	fun interpret(node: RootNode)
}
