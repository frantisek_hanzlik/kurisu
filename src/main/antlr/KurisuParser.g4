parser grammar KurisuParser;

options { tokenVocab = KurisuLexer; }

expressionDelimiter: NEWLINE | WHITESPACE ;

file: (expressions+=expression expressionDelimiter)* expressions+=expression ;

expression
	: operandLeft=expression operator=(OPERATOR_MULTIPLICATION | OPERATOR_DIVISION) operandRight=expression # expressionOperationBinary
	| operandLeft=expression operator=(OPERATOR_ADDITION | OPERATOR_SUBTRACTION) operandRight=expression # expressionOperationBinary
	| operator=OPERATOR_SUBTRACTION operand=expression # expressionOperationUnary
	| PARENTHESIS_LEFT expression PARENTHESIS_RIGHT # expressionParenthesis
	| numberLiteral # expressionNumberLiteral
	;

numberLiteral
	: LITERAL_NUMBER_INTEGER # numberLiteralInteger
	| LITERAL_NUMBER_DECIMAL # numberLiteralDecimal
	;
