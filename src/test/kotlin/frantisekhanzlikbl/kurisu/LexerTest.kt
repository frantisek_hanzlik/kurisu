package frantisekhanzlikbl.kurisu

import frantisekhanzlikbl.kurisu.antlr.generated.KurisuLexer
import org.antlr.v4.runtime.CharStreams
import org.antlr.v4.runtime.CommonTokenStream
import org.antlr.v4.runtime.Lexer
import org.antlr.v4.runtime.Token
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class LexerTest {
	@Nested inner class Literals {
		@Nested inner class Integers {
			@Test fun `single digit`() = createLexerAndAssertTokens("5", listOf(KurisuLexer.LITERAL_NUMBER_INTEGER to "5", CommonTokens.eof))
			@Test fun `multiple digits`() = createLexerAndAssertTokens("189", listOf(KurisuLexer.LITERAL_NUMBER_INTEGER to "189", CommonTokens.eof))
		}

		@Nested inner class Decimals {
			@Test fun `starting with zero`() = createLexerAndAssertTokens("0.1", listOf(KurisuLexer.LITERAL_NUMBER_DECIMAL to "0.1", CommonTokens.eof))
			@Test fun `multiple digits`() = createLexerAndAssertTokens("18.96", listOf(KurisuLexer.LITERAL_NUMBER_DECIMAL to "18.96", CommonTokens.eof))
		}
	}

	@Nested inner class SimpleExpressions {
		@Test fun `single operation`() = createLexerAndAssertTokens(
			"12 + 0.9",
			listOf(
				KurisuLexer.LITERAL_NUMBER_INTEGER to "12",
				KurisuLexer.OPERATOR_ADDITION to "+",
				KurisuLexer.LITERAL_NUMBER_DECIMAL to "0.9",
				CommonTokens.eof
			)
		)

		@Test fun `multiple operations`() = createLexerAndAssertTokens(
			"0.62 *5*(-8/ 2)",
			listOf(
				KurisuLexer.LITERAL_NUMBER_DECIMAL to "0.62",
				KurisuLexer.OPERATOR_MULTIPLICATION to "*",
				KurisuLexer.LITERAL_NUMBER_INTEGER to "5",
				KurisuLexer.OPERATOR_MULTIPLICATION to "*",
				KurisuLexer.PARENTHESIS_LEFT to "(",
				KurisuLexer.OPERATOR_SUBTRACTION to "-",
				KurisuLexer.LITERAL_NUMBER_INTEGER to "8",
				KurisuLexer.OPERATOR_DIVISION to "/",
				KurisuLexer.LITERAL_NUMBER_INTEGER to "2",
				KurisuLexer.PARENTHESIS_RIGHT to ")",
				CommonTokens.eof
			)
		)
	}

	companion object {
		object CommonTokens {
			val eof = KurisuLexer.EOF to "<EOF>"
		}

		private fun getLexerTokens(lexer: Lexer): List<Token> = CommonTokenStream(lexer).run {
			fill()
			tokens
		}

		private fun assertLexerTokens(lexer: Lexer, tokens: List<Pair<Int, String>>) {
			Assertions.assertIterableEquals(
				tokens.map { (tokenType, tokenText) -> Pair(KurisuLexer.VOCABULARY.getSymbolicName(tokenType), tokenText) },
				getLexerTokens(lexer).map { token -> Pair(KurisuLexer.VOCABULARY.getSymbolicName(token.type), token.text) }
			)
		}

		fun createLexerAndAssertTokens(string: String, tokens: List<Pair<Int, String>>) {
			assertLexerTokens(KurisuLexer(CharStreams.fromString(string)), tokens)
		}
	}
}
